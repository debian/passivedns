Source: passivedns
Maintainer: Axel Beckert <abe@debian.org>
Uploaders: Sascha Steinbiss <satta@debian.org>
Section: net
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libpcap-dev,
               libssl-dev,
               libldns-dev,
               txt2man
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/debian/passivedns
Vcs-Git: https://salsa.debian.org/debian/passivedns.git
Homepage: https://github.com/gamelinux/passivedns
Rules-Requires-Root: no

Package: passivedns
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends},
         lsb-base
Recommends: procps
Description: network sniffer that logs all DNS server replies for use in a passive DNS setup
 A tool to collect DNS records passively to aid Incident handling, Network
 Security Monitoring (NSM) and general digital forensics.
 .
 PassiveDNS sniffs traffic from an interface or reads a pcap-file and outputs
 the DNS-server answers to a log file. PassiveDNS can cache/aggregate duplicate
 DNS answers in-memory, limiting the amount of data in the logfile without
 losing the essense in the DNS answer.
 .
 PassiveDNS works on IPv4 and IPv6 traffic and parse DNS traffic over TCP and
 UDP.
